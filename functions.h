#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <QVector>

bool compare(QVector<QVector<int>> &vec1, QVector<QVector<int>> &vec2);
QVector<int> search(QVector<QVector<int>>& vec, int elem);
QVector<int> find3max(QVector<QVector<int>>& vec);
int diagsum(QVector<QVector<int>> &vec);
int evenindsum(QVector<QVector<int>> &vec);
QVector<QVector<int>> mirror(QVector<QVector<int>> &vec);

#endif // FUNCTIONS_H
