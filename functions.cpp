#include "functions.h"
#include <QVector>
#include <iostream>
using namespace std;

bool compare(QVector<QVector<int>> &vec1, QVector<QVector<int>> &vec2)
{
   if (vec1.size() == vec2.size())
   {
       for (int i = 0; i < vec1.size(); i++)
       {
           if (vec1[i] == vec2[i]) continue;
           else return false;
       }
       return true;
   }
   return false;
}

QVector<int> search(QVector<QVector<int>> &vec, int elem)
{
   // int elem;
    int ind1 = -1, ind2 = -1;
  //  cin >> elem;

    for (int i = 0; i < vec.size(); i++)
        for (int j = 0; j < vec[i].size(); j++)
        {
            if (vec[i][j] == elem) { ind1 = i; ind2 = j; }
        }

    QVector<int> index = { ind1, ind2 };
    return index;
}

QVector<int> find3max(QVector<QVector<int>> &vec)
{
    int max1 = vec[0][0];
    int max2 = vec[0][0];
    int max3 = vec[0][0];
    int k = 0, l = 0, m = 0, n = 0;

    for (int z = 0; z < 3; z++)
    for (int i = 0; i < vec.size(); i++)
        for (int j = 0; j < vec[i].size(); j++)
        {
            if (vec[i][j] >= max1) { max1 = vec[i][j]; k = i; l = j; }
            if ((vec[i][j] >= max2) && (i != k) && (j != l))  { max2 = vec[i][j]; m = i; n = j; }
            if ((vec[i][j] >= max3) && (i != k) && (i != m) && (j != l) && (j != n)) max3 = vec[i][j];

        }

    QVector<int> max = {max1, max2, max3};
    return max;
}



int diagsum(QVector<QVector<int>> &vec)
{
    int sum1 = 0, sum2 = 0;
    int sum = 0;
    for (int i = 0; i < vec.size(); i++)
        {
            sum1 += vec[i][i];
            sum2 += vec[i][vec[i].size()-1-i];
        }
    sum = sum1 + sum2;

    return sum;
}

int evenindsum(QVector<QVector<int>> &vec)
{
    int sum = 0;
    for (int i = 0; i < vec.size(); i++)
        for (int j = 0; j < vec[i].size(); j++)
        {
            if ((i + j)%2 == 0) sum += vec[i][j];
        }
    return sum;
}


QVector<QVector<int>> mirror(QVector<QVector<int>> &vec)
{
   // QVector<QVector<int>> trvec;
    for (int i = 0; i < vec.size(); i++)
        for (int j = 0; j < vec[i].size()/2; j++)
        {
            if (vec[i].size()%2 == 1)
                vec[i][vec.size()/2 + j] = vec[i][vec.size()/2 - j];

            if (vec[i].size()%2 == 0)
                vec[i][vec.size()/2 + j] = vec[i][vec.size()/2 - 1 - j];
        }
    return vec;
}
