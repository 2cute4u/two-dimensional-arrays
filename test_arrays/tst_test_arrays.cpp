#include <QtTest>
#include "tst_test_arrays.h"
#include "../functions.h"

test_arrays::test_arrays()
{

}

test_arrays::~test_arrays()
{

}

void test_arrays::test_compare()
{
    QVector<QVector<int>> v1 = {{1, 2}, {3, 4, 5}};
    QVector<QVector<int>> v2 = {{1, 2}, {3, 4, 5}};
    QCOMPARE(true, compare(v1, v2));

    v1 = {{1, 2}, {3, 4, 5}};
    v2 = {{1, 8}, {3, 4, 5}};
    QCOMPARE(false, compare(v1, v2));

    v1 = {{1, 2}, {3, 4, 5}};
    v2 = {{1, 2}, {3, 4, 5}, {6, 7}};
    QCOMPARE(false, compare(v1, v2));

    v1 = {{1, 2}, {3, 4, 5}};
    v2 = {{1, 2}, {1, 2, 3}};
    QCOMPARE(false, compare(v1, v2));
}

void test_arrays::test_search()
{
    QVector<QVector<int>> v = {{4, 46, 78},
                               {99, 65, 23},
                               {17, 3, 10}};
    QVector<int> i = {1, 0};
    QCOMPARE(i, search(v, 99));

    v = {{4, 46, 78},
         {99, 65, 23},
         {17, 3, 10}};
    i = {2, 2};
    QCOMPARE(i, search(v, 10));

    v = {{4, 46, 78},
         {99, 65, 23},
         {17, 3, 10}};
    i = {0, 0};
    QCOMPARE(i, search(v, 4));
}

void test_arrays::test_max3()
{
    QVector<QVector<int>> v = {{4, 46, 78},
                               {99, 65, 23},
                               {17, 3, 10}};
    QVector<int> max = {99, 78, 65};
    QCOMPARE(max, find3max(v));

    v = {{0, 0, 0},
         {0, 0, 0},
         {0, 0, 0}};
    max = {0, 0, 0};
    QCOMPARE(max, find3max(v));

    v = {{-23, 0, -1},
         {1, 0, -5},
         {0, 0, 1}};
    max = {1, 0, -1};
    QCOMPARE(max, find3max(v));

    v = {{-23, -78, -1},
         {-2, -25, -5},
         {-9, -8, -11}};
    max = {-1, -2, -5};
    QCOMPARE(max, find3max(v));
}

void test_arrays::test_diagsum()
{
    QVector<QVector<int>> v = {{4, 46, 78},
                               {99, 65, 23},
                               {17, 3, 10}};
    QCOMPARE(239, diagsum(v)); //174

    v = {{-23, 0, -1},
         {1, 0, -5},
         {0, 0, 1}};
    QCOMPARE(-23, diagsum(v));

    v = {{-23, 0, -1},
         {1, 0, -5},
         {0, 0, 23},
         {2, 3, 4}};
    QCOMPARE(-1, diagsum(v));

    v = {{-23, 0, -1, 5},
         {1, 0, -5, 6},
         {0, 0, 1, 17}};
    QCOMPARE(-22, diagsum(v));

}

void test_arrays::test_evenindsum()
{
    QVector<QVector<int>> v = {{4, 46, 78},
                               {99, 65, 23},
                               {17, 3, 10}};
    QCOMPARE(174, evenindsum(v));

    v = {{-23, 0, -1},
         {1, 0, -5},
         {0, 0, 1}};
    QCOMPARE(-23, evenindsum(v));

    v = {{-23, 0, -1},
         {1, 0, -5},
         {0, 0, 23},
         {2, 3, 4}};
    QCOMPARE(2, evenindsum(v));

    v = {{-23, 0, -1, 5},
         {1, 0, -5, 6},
         {0, 0, 1, 17}};
    QCOMPARE(-17, evenindsum(v));
}

void test_arrays::test_mirroring()
{
    QVector<QVector<int>> bef = {{4, 46, 78},
                                 {99, 65, 23},
                                 {17, 3, 10}};

    QVector<QVector<int>> aft = {{4, 46, 4},
                                 {99, 65, 99},
                                 {17, 3, 17}};
    QCOMPARE(aft, mirror(bef));


    bef = {{-23, 0, -1, 5},
           {1, 0, -5, 6},
           {13, 0, 1, 17}};

    aft = {{-23, 0, 0, -23},
           {1, 0, 0, 1},
           {13, 0, 0, 13}};

    QCOMPARE(aft, mirror(bef));


    bef = {{1, 2, 3, 4, 5},
           {6, 7, 8, 9, 10},
           {11, 12, 13, 14 ,15}};

    aft = {{1, 2, 3, 2, 1},
           {6, 7, 8, 7, 6},
           {11, 12, 13, 12, 11}};

    QCOMPARE(aft, mirror(bef));
}
