#ifndef TST_TEST_ARRAYS_H
#define TST_TEST_ARRAYS_H

#include <QtTest>

class test_arrays : public QObject
{
    Q_OBJECT

public:
    test_arrays();
    ~test_arrays();

private slots:
    void test_compare();
    void test_search();
    void test_max3();
    void test_diagsum();
    void test_evenindsum();
    void test_mirroring();

};

#endif // TST_TEST_ARRAYS_H
